<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'proto1' );

/** Имя пользователя MySQL *//**/
define( 'DB_USER', 'proto' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'rbzirj25' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*EjjYD23{9=`V,h |7CHZq=@cFB1q]-Gjn~|Td&8bH;I~{jiqkX~&}#Kf3%jlt,,' );
define( 'SECURE_AUTH_KEY',  'g(fDzFEp(&e Z*>pxq_#5f9d}UeQv/>Y0sH9a.x@~xQ>:#Ge]z[787-Q|t!cyh_i' );
define( 'LOGGED_IN_KEY',    '=5_XrNe-{m[)ut[jmQq2#M#qkKoILQ:|hweINxPv{GDe]QJ:OzqRFEAlH)(B=X%:' );
define( 'NONCE_KEY',        'x62:O+[)|dYg>AJbAPiHC];j.RRtOE!z-L_Tp;WMdX|f(wgtTUc%XH080;XuEx%O' );
define( 'AUTH_SALT',        '|4CnW5Ny6z!# .tcvznb;a;VhrpM]iQ@3@%U. (bgL(.Qj8i6OXM]>lj/zj<e=QV' );
define( 'SECURE_AUTH_SALT', ':s(6}9bzb*)wWC|Z::01q3lg>O]Ff3Tqyqwuii{h8BuNP/liXY8|Ahdtl<$Z[n$L' );
define( 'LOGGED_IN_SALT',   'r(8-:!b!G0jl}_3cCQrI6ngBrYJ0+0E)l4:X[)bfVN1C)5@W2?E<OG`1GD]dv(If' );
define( 'NONCE_SALT',       '(mhl#W8D`hq1_g#j5D9$!V_o</hY(a@67yO$2-FlHM9F4b-+.zfAq6wx|_rogQ[ ' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );     // включение дебаг режима
define( 'WP_DEBUG_LOG', '/tpm/relotis.com.log' ); // true - логирование ошибок в файл /wp-content/debug.log
define( 'WP_DEBUG_DISPLAY', false ); // false - отключает показ ошибок на экране
define( 'SCRIPT_DEBUG', true ); // используем полные версии JS и CSS файлов движка
/* Это всё, дальше не редактируем. Успехов! */

set_error_handler('error_handler');
function error_handler($level, $message, $file, $line)
{
    $log = '/var/www/proto1/wp-content/debug.log';
    $date = date("Y.m.d H:j", time());
    $contents = "msg={$message} | file={$file} | line={$line} | " . PHP_EOL;
    file_put_contents($log, $contents, FILE_APPEND);
}

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
define( 'AUTOMATIC_UPDATER_DISABLED', true );
/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
